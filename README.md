# W3D Hub Server Browser

This is a simple Flutter application which downloads and displays the available game servers from the W3D Hub Game Server Hub service.

For now I am using this as a test bed to learn more about how Flutter _(and Dart)_ works and experiment with what it can do. As such code in this repository should not be considered production quality, or a even necessaraily a good way of doing things. _Caveat emptor_.

Feel free to suggest features (by creating an issue) or submit small MRs to fix bugs or add features - especially if you can point out a better way of doing something that I've overlooked in my learning about Flutter / Dart.
