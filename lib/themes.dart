import 'package:flutter/material.dart';

// Define the W3D Hub dark theme primary material
const w3dhubThemeDarkPrimary = MaterialColor(0xFF000000, {
  50: Color(0xFF000000),
  100: Color(0xFF000000),
  200: Color(0xFF000000),
  300: Color(0xFF000000),
  400: Color(0xFF000000),
  500: Color(0xFF000000),
  600: Color(0xFF000000),
  700: Color(0xFF000000),
  800: Color(0xFF000000),
  900: Color(0xFF000000)
});

// Define the W3D Hub dark theme colours
final w3dhubThemeDarkColours = ColorScheme.fromSwatch(
        primarySwatch: w3dhubThemeDarkPrimary, brightness: Brightness.dark)
    .copyWith(secondary: const Color(0xFFE67B09));

// Define the W3D Hub dark theme
final w3dhubDarkTheme = ThemeData(colorScheme: w3dhubThemeDarkColours);
