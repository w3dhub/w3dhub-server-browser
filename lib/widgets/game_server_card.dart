import 'package:flutter/material.dart';

class GameServerCard extends StatefulWidget {
  const GameServerCard({Key? key, required this.serverTitle}) : super(key: key);

  final String serverTitle;

  @override
  _GameServerCardState createState() => _GameServerCardState();
}

class _GameServerCardState extends State<GameServerCard> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              leading: const Icon(Icons.games),
              title: Text(widget.serverTitle),
              subtitle: const Text('Some information about a server'),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                TextButton(
                  child: const Text('Wibble'),
                  onPressed: () {/* ... */},
                ),
                const SizedBox(width: 8),
                TextButton(
                  child: const Text('Wobble'),
                  onPressed: () {/* ... */},
                ),
                const SizedBox(width: 8),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
