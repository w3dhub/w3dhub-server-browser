import 'package:event/event.dart';
import '../gsh_http_client.dart';

class GameServersService {
  final serversUpdatedEvent = Event();

  Future<List<ServerData>> getServers() {
    return GshHttpClient().getServers();
  }
}
