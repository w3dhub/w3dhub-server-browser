import 'dart:convert';

import 'package:http/http.dart' as http;

class ServerData {
  final String id;

  final String? game;

  final ServerStatus status;

  ServerData({required this.id, this.game, required this.status});

  factory ServerData.fromJson(Map<String, dynamic> json) {
    return ServerData(
        id: json['id'],
        game: json['game'],
        status: ServerStatus.fromJson(json['status']));
  }
}

class ServerStatus {
  final String name;

  final int currentPlayers;

  final int maxPlayers;

  ServerStatus(
      {required this.name,
      required this.currentPlayers,
      required this.maxPlayers});

  factory ServerStatus.fromJson(Map<String, dynamic> json) {
    return ServerStatus(
        name: json['name'],
        currentPlayers: json['numplayers'] ?? 0,
        maxPlayers: json['maxplayers']);
  }
}

class GshHttpClient {
  Future<List<ServerData>> getServers() async {
    http.Response response = await http.get(
        Uri.parse('https://gsh.w3dhub.com/listings/getAll/v2?statusLevel=1'));

    if (response.statusCode != 200) {
      throw Exception('Failed to load album');
    }

    Iterable l = json.decode(response.body);
    return List<ServerData>.from(l.map((model) => ServerData.fromJson(model)));
  }
}
